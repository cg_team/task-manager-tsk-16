package ru.inshakov.tm.api.controller;

public interface IProjectTaskController {
    void showAllTasksByProjectId();

    void bindTaskByProject();

    void unbindTaskFromProject();

    void removeProjectById();
}
