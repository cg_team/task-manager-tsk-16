package ru.inshakov.tm.api.service;

import ru.inshakov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
