package ru.inshakov.tm.exception.system;

public class UndefinedErrorException extends RuntimeException {

    public UndefinedErrorException() {
        super("Undefined Error. please contact your administrator");
    }

}
