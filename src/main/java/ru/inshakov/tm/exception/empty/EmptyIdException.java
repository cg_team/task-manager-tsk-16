package ru.inshakov.tm.exception.empty;

public class EmptyIdException extends RuntimeException {

    public EmptyIdException() {
        super("Error! Id is empty...");
    }

}
