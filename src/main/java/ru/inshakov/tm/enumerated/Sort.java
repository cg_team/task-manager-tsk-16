package ru.inshakov.tm.enumerated;

import ru.inshakov.tm.comparator.ComparatorByCreated;
import ru.inshakov.tm.comparator.ComparatorByDateStart;
import ru.inshakov.tm.comparator.ComparatorByName;
import ru.inshakov.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    DATE_START("Sort by date start", ComparatorByDateStart.getInstance()),
    NAME("Sort by name", ComparatorByName.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    private final String displayName;

    private final Comparator comparator;


}
