package ru.inshakov.tm.bootstrap;

import ru.inshakov.tm.api.controller.ICommandController;
import ru.inshakov.tm.api.controller.IProjectTaskController;
import ru.inshakov.tm.api.repository.ICommandRepository;
import ru.inshakov.tm.api.service.*;
import ru.inshakov.tm.api.controller.IProjectController;
import ru.inshakov.tm.api.repository.IProjectRepository;
import ru.inshakov.tm.api.controller.ITaskController;
import ru.inshakov.tm.api.repository.ITaskRepository;
import ru.inshakov.tm.constant.ArgumentConst;
import ru.inshakov.tm.constant.TerminalConst;
import ru.inshakov.tm.controller.CommandController;
import ru.inshakov.tm.controller.ProjectController;
import ru.inshakov.tm.controller.ProjectTaskController;
import ru.inshakov.tm.controller.TaskController;
import ru.inshakov.tm.exception.system.UnknownArgumentException;
import ru.inshakov.tm.exception.system.UnknownCommandException;
import ru.inshakov.tm.repository.CommandRepository;
import ru.inshakov.tm.repository.ProjectRepository;
import ru.inshakov.tm.repository.TaskRepository;
import ru.inshakov.tm.service.*;
import ru.inshakov.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final ILoggerService loggerService = new LoggerService();

    public void run(final String... args) {
        loggerService.debug("TEST!!11");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try{
                parseCommand(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.ARG_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showSystemInfo();
                break;
            default:
                showIncorrectArgument(arg);
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.CMD_HELP:
                commandController.showHelp();
                break;
            case TerminalConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.CMD_INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.CMD_EXIT:
                commandController.exit();
                break;
            case TerminalConst.CMD_TASK_LIST:
                taskController.showList();
                break;
            case TerminalConst.CMD_TASK_CREATE:
                taskController.create();
                break;
            case TerminalConst.CMD_TASK_CLEAR:
                taskController.clear();
                break;
            case TerminalConst.CMD_SHOW_PROJECT_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.CMD_SHOW_PROJECT_BY_NAME:
                projectController.showProjectByName();
                break;
            case TerminalConst.CMD_REMOVE_PROJECT_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.CMD_REMOVE_PROJECT_BY_NAME:
                projectController.removeProjectByName();
                break;
            case TerminalConst.CMD_REMOVE_PROJECT_BY_ID:
                projectTaskController.removeProjectById();
                break;
            case TerminalConst.CMD_SHOW_PROJECT_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.CMD_UPDATE_PROJECT_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.CMD_UPDATE_PROJECT_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.CMD_PROJECT_LIST:
                projectController.showList();
                break;
            case TerminalConst.CMD_PROJECT_CREATE:
                projectController.create();
                break;
            case TerminalConst.CMD_PROJECT_CLEAR:
                projectController.clear();
                break;
            case TerminalConst.CMD_START_TASK_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.CMD_START_TASK_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.CMD_START_TASK_BY_NAME:
                taskController.startTaskByName();
                break;
            case TerminalConst.CMD_FINISH_TASK_BY_ID:
                taskController.finishTaskById();
                break;
            case TerminalConst.CMD_FINISH_TASK_BY_INDEX:
                taskController.finishTaskByIndex();
                break;
            case TerminalConst.CMD_FINISH_TASK_BY_NAME:
                taskController.finishTaskByName();
                break;
            case TerminalConst.CMD_START_PROJECT_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.CMD_START_PROJECT_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.CMD_START_PROJECT_BY_NAME:
                projectController.startProjectByName();
                break;
            case TerminalConst.CMD_FINISH_PROJECT_BY_ID:
                projectController.finishProjectById();
                break;
            case TerminalConst.CMD_FINISH_PROJECT_BY_INDEX:
                projectController.finishProjectByIndex();
                break;
            case TerminalConst.CMD_FINISH_PROJECT_BY_NAME:
                projectController.finishProjectByName();
                break;
            case TerminalConst.CMD_SHOW_TASKS_BY_PROJECT_ID:
                projectTaskController.showAllTasksByProjectId();
                break;
            case TerminalConst.CDM_BIND_TASK_BY_PROJECT:
                projectTaskController.bindTaskByProject();
                break;
            case TerminalConst.CDM_UNBIND_TASK_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            default:
                showIncorrectCommand(command);
        }
    }

    public void showIncorrectArgument(String arg) {
        throw new UnknownArgumentException(arg);
    }

    public void showIncorrectCommand(String command) {
        throw new UnknownCommandException(command);
    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        String arg = args[0];
        parseArg(arg);
        return true;
    }

}
